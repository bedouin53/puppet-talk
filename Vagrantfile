# -*- mode: ruby -*-
# vi: set ft=ruby :

CPUS = 1
MEMORY = 1024

master_node = {
  :hostname => 'master.puppet.vm',
  :ip => '172.16.32.20',
  :box => 'centos/7',
  :ram => MEMORY,
  :cpus => CPUS,
  :fwdhost => 8140,
  :fwdguest => 8140,
  :script => 'scripts/master-vm-initialization.sh'
}

web_node = {
  :hostname => 'web.puppet.vm',
  :ip => '172.16.32.21',
  :box => 'ubuntu/hirsute64',
  :ram => MEMORY,
  :cpus => CPUS,
  :script => 'scripts/web-vm-initialization.sh'
}

# Official Centos 7
# config.vm.box = "centos/7"
# Official Ubuntu 21.04 Hirsute Hippo
# config.vm.box = "ubuntu/hirsute64"
# Official Ubuntu 18.04 LTS Bionic Beaver
# config.vm.box = "ubuntu/bionic64"

Vagrant.configure("2") do |config|

  config.vm.define "master" do |master|
    master.vm.box = master_node[:box]
    master.vm.hostname = master_node[:hostname]
    master.vm.provision :shell, :path => master_node[:script]
    master.vm.network :private_network, ip: master_node[:ip]

    if master_node[:fwdhost]
      master.vm.network :forwarded_port, guest: master_node[:fwdguest], host: master_node[:fwdhost]
    end

    master.vm.provider "virtualbox" do |vb|
      vb.gui = true
      vb.memory = master_node[:ram]
      vb.cpus = master_node[:cpus]
      vb.name = master_node[:hostname]
    end
  end

  config.vm.define "web" do |web|
    web.vm.box = web_node[:box]
    web.vm.hostname = web_node[:hostname]
    web.vm.provision :shell, :path => web_node[:script]
    web.vm.network :private_network, ip: web_node[:ip]

    web.vm.provider "virtualbox" do |vb|
      vb.gui = true
      vb.memory = web_node[:ram]
      vb.cpus = web_node[:cpus]
      vb.name = web_node[:hostname]
    end
  end

end
