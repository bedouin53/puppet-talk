#Puppet sharing experience

## Resources
### What you will need
- I'm using [Virtual Box](https://www.virtualbox.org/wiki/Downloads) to start the different node
- I'm using [Vagrant](https://www.vagrantup.com/) to start and prevision my virtualbox vm node
- If you want to see list of available [boxes](https://app.vagrantup.com/boxes/search) for vagrant

### Puppet docs
- The puppet [website](https://puppet.com/)
- The list of puppet built-in [types](https://puppet.com/docs/puppet/6/type.html)
- [Puppet forge](https://forge.puppet.com/) to find resources built by the community
- [R10K](http://www.geoffwilliams.me.uk/puppet/wtf_is_r10k) helper to manage the contents of the puppet environments' directory

## Puppet what is it ?
- **For** Sys Admin or DevOps enthusiast
- **That needs** to configure and maintain configuration of hundred machine
- **Our product** puppet
- **Offers** infrastructure as code for multi OS tool
- **Like** Chef, Ansible or Salt
- **Unlike** handwrite script

## Why I like Puppet ?
Puppet focus on modeling your infra by describing the desired state of each node.
At PTL we are using Terraform (same dev as vagrant) to provision our AWS machine.
Puppet is quite the same but more for enterprise infrastructure.

To describe the infrastructure puppet is using this terms :
- `Node` is an individual service or device (ie each server physic or logic is a node)
- `Resources` are individual units of configuration (like for handling files, system users)
- `Classes` group together `Resources` that makes sense as a logical group under a name that can be included in other code
- `Manifest` text file holding Puppet code (.pp), should contain a single `Class` or defined `Resource`
- `Profile` a `Class` that defines a specific set of configuration
- `Role` a `Class` that defines the business role of a `Node`
   - `Role` are generally build of several `Profile` classes
   - The `Role` should answer to a simple question : "What's this node ?", and the answer should be simple like "it's a dev station" "it's a production tomcat server"

## Puppet Forge
[Puppet forge](https://forge.puppet.com/) to find resources built by the community

You can find 4 type of modules :
- `Supported` -> Written and maintained by Puppet
- `Partner` -> Tested and confirmed to work with Puppet enterprise, but maintained by partner company (generally the maker of the software)
- `Approved` -> Review and approved by puppet
- `Community` -> All the rest (quality can vary, but some of the most downloaded modules are in this category)

