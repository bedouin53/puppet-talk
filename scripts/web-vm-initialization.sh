#!/bin/bash

red='\e[0;31m'
orange='\e[0;33m'
green='\e[0;32m'
end='\e[0m'

echo -e "----> ${orange}Install puppet and others${end}"
wget https://apt.puppetlabs.com/puppet7-release-focal.deb
dpkg -i puppet7-release-focal.deb
apt-get update -y
apt-get install puppet-agent -y
echo -e "----> ${orange}End of Install puppet and others${end}"


echo -e "----> ${green}# Update hosts${end}"
echo "172.16.32.20 master.puppet.vm master puppet" >> /etc/hosts
echo "172.16.32.21 web.puppet.vm web" >> /etc/hosts

echo -e "----> ${green}# Create alias${end}"
echo "alias apply='/opt/puppetlabs/bin/puppet agent -t'" >> /root/.bashrc

echo -e "----> ${green}# Update config of node to point on master${end}"
echo "" >> /etc/puppetlabs/puppet/puppet.conf
echo "[main]" >> /etc/puppetlabs/puppet/puppet.conf
echo "certname = web.puppet.vm" >> /etc/puppetlabs/puppet/puppet.conf
echo "server = master.puppet.vm" >> /etc/puppetlabs/puppet/puppet.conf

systemctl status puppet
systemctl start puppet
systemctl enable puppet
/opt/puppetlabs/bin/puppet agent --test
echo -e "----> ${green}# Update config of node to point on master${end}"
