#!/bin/bash

red='\e[0;31m'
orange='\e[0;33m'
green='\e[0;32m'
end='\e[0m'

echo -e "----> ${orange}Install puppet and others${end}"
rpm -Uvh https://yum.puppet.com/puppet7-release-el-7.noarch.rpm
#rpm -Uvh https://yum.puppet.com/puppet6-release-el-7.noarch.rpm
yum install -y puppetserver vim git
echo -e "----> ${orange}End of Install puppet and others${end}"


echo -e "----> ${orange}Puppet master server (config and start)${end}"
echo -e "----> ${green}# Less memory for puppetserver${end}"
sed -i 's/2g/512m/g' /etc/sysconfig/puppetserver

echo -e "----> ${green}# start puppetserver${end}"
systemctl start puppetserver
systemctl status puppetserver


echo -e "----> ${green}# Update hosts${end}"
echo "172.16.32.20 master.puppet.vm master" >> /etc/hosts
echo "172.16.32.21 web.puppet.vm web" >> /etc/hosts

echo -e "----> ${green}# enable puppetserver${end}"
systemctl enable puppetserver

echo -e "----> ${green}# Update config of master to point on himself${end}"
echo "" >> /etc/puppetlabs/puppet/puppet.conf
echo "[agent]" >> /etc/puppetlabs/puppet/puppet.conf
echo "server = master.puppet.vm" >> /etc/puppetlabs/puppet/puppet.conf

echo -e "----> ${green}# Add ruby in the path${end}"
echo "" >> /etc/bashrc
echo "PATH=$PATH:/opt/puppetlabs/puppet/bin" >> /etc/bashrc
echo "export PATH" >> /etc/bashrc
echo "alias apply='r10k deploy environment -p && puppet agent -t'" >> /etc/bashrc
source .bash_profile
echo -e "----> ${orange}End of Puppet master server (config and start)${end}"

echo -e "----> ${orange}Check install${end}"
echo -e "----> ${green}gem version${end}"
gem -v
echo -e "----> ${green}puppet agent try${end}"
puppet agent -t
echo -e "----> ${orange}End of Check install${end}"


echo -e "----> ${orange}Install r10k - use for git update${end}"
echo -e "----> ${green}gem install r10k${end}"
gem install r10k
echo -e "----> ${green}Config r10k${end}"
mkdir /etc/puppetlabs/r10k
touch /etc/puppetlabs/r10k/r10k.yaml
echo "---" >> /etc/puppetlabs/r10k/r10k.yaml
echo "" >> /etc/puppetlabs/r10k/r10k.yaml
echo ":sources:" >> /etc/puppetlabs/r10k/r10k.yaml
echo "  :thomasbedu-fr:" >> /etc/puppetlabs/r10k/r10k.yaml
echo "    remote: 'https://gitlab.com/bedouin53/puppet-talk.git'" >> /etc/puppetlabs/r10k/r10k.yaml
echo "    basedir: '/etc/puppetlabs/code/environments'" >> /etc/puppetlabs/r10k/r10k.yaml

echo -e "----> ${green}deploy r10k on machine${end}"
r10k deploy environment -p
ls -all /etc/puppetlabs/code/environments/production

echo -e "----> ${orange}End of Install r10k - use for git update${end}"

echo -e "----> ${orange}First call to puppet agent${end}"
puppet agent -t
echo -e "----> ${orange}End of First call to puppet agent${end}"

echo -e "----> ${orange}Check certificate${end}"
/opt/puppetlabs/bin/puppetserver ca list --all
/opt/puppetlabs/bin/puppetserver ca sign --all
echo -e "----> ${orange}End of Check certificate${end}"

