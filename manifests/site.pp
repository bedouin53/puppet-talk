# site.pp is where the puppet master first looks for configuration details about a system when the puppet agent checks in

# As for the moment we just have the master node we can use the default node
# A node will match
node default {

  # 1 - Create a file
  # 2 - set owner to root and see that nothing change
  # 3 - create another  file { '/root/README':
  file { '/root/README': /* id of the resource */
    ensure  => file, /* file/folder/link/absent */
    content => 'This is my readme',
  }

}

node 'master.puppet.vm' {
  include role::master_server
}

node /^web/ {
  include role::app_server
}

